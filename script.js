const mocked_posts = [
    {
        ID: 0,
        Text: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nemo modi animi sit distinctio dolore fugiat ab, porro deleniti. Obcaecati accusamus exercitationem officia repellendus qui deleniti repudiandae voluptatem voluptate. Eaque, maxime!',
        Author: 'Захардкоженный пост',
        Picture: 'https://storage.theoryandpractice.ru/tnp/uploads/image_unit/000/156/586/image/base_87716f252d.jpg',
        Comments: [
            {
                ID:0,
                PostID:0,
                Author: 'Автор',
                Text: 'Коммент'
            },
            {
                ID:1,
                PostID:0,
                Author: 'Ещё автор',
                Text: 'Ещё коммент'
            }
        ]
    },
]

function makeDefaultElement(type, classes, content) {
    let elem = document.createElement(type);
    elem.classList.add(classes);
    elem.append(content);

    return elem
}

function makeElementFromPost(post_data) {
    let post = document.createElement("div");
    post.classList.add('post');
    let postAuthor = document.createElement("p");
    postAuthor.classList.add('post__author');
    postAuthor.append(post_data.Author);
    let postText = document.createElement("p");
    postText.classList.add('post__text');
    postText.append(post_data.Text);
    let image = document.createElement("img");
    image.classList.add('post__picture');
    image.src = post_data.Picture;
    post.append(postAuthor, postText, image, makeElementFromComments(post_data.Comments));

    return post
}

function makeElementFromComments(comments_data) {
    let comments = document.createElement("ul");
    comments.classList.add('comments');
    for (let comment_data of comments_data) {
        let comment = document.createElement("li");
        comment.classList.add('comments__comment');
        comment.append(`${comment_data.Author}: ${comment_data.Text}`);
        comments.append(comment);
    }
    console.log('comments', comments)
    return comments
}

function makeCommentForm(post_id) {
    let form = document.createElement("form");
    form.classList.add('add-comment');
    let input_text = document.createElement("input");
    input_text.classList.add('add-comment__text');
    postAuthor.append(post_data.Author);
}

function addComment(post_id) {

}

async function funonload() {
    list = document.getElementById("posts-list");
    let posts = await fetch('http://5.188.142.205:8080/posts', {
        method: 'get'
    })
    data = [...mocked_posts, ...(await posts.json())]
    console.log(data)
    for (let post_data of data) {
        list.append(makeElementFromPost(post_data));
    }
}